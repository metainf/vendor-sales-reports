/*
 * #%L
 * Vendor Sales Reports
 * %%
 * Copyright (C) 2014 - 2016 META-INF KFT
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 * #L%
 */
package com.metainf.atlassian.sales;

import static org.hamcrest.Matchers.is;
import static org.joda.time.DateTimeConstants.DECEMBER;
import static org.joda.time.DateTimeConstants.FEBRUARY;
import static org.joda.time.DateTimeConstants.APRIL;
import static org.joda.time.DateTimeConstants.JANUARY;
import static org.joda.time.DateTimeConstants.JULY;
import static org.joda.time.DateTimeConstants.MONDAY;
import static org.joda.time.DateTimeConstants.MARCH;
import static org.joda.time.DateTimeConstants.SUNDAY;
import static org.junit.Assert.assertThat;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.junit.Before;
import org.junit.Test;

/**
 * Verifies the logic for calculating reporting interval based on a period
 */
public class TestPeriod {
	private DateTime now;

	@Before
	public void setUp() {
		now = new DateTime();
	}

	@Test
	public void getIntervalAll() {
		Interval interval = Period.ALL.getInterval();
		assertThat(interval.getStart().getYear(), is(2000));
		//
		assertThat(interval.getEnd().getYear(), is(now.getYear()));
		assertThat(interval.getEnd().getMonthOfYear(), is(now.getMonthOfYear()));
		assertThat(interval.getEnd().getDayOfMonth(), is(now.getDayOfMonth()));
		assertThat(interval.getEnd().getHourOfDay(), is(23));
	}

	@Test
	public void getIntervalThisYear() {
		Interval interval = Period.THIS_YEAR.getInterval();
		assertThat(interval.getStart().getYear(), is(now.getYear()));
		assertThat(interval.getEnd().getYear(), is(now.getYear()));
		//
		assertThat(interval.getStart().getMonthOfYear(), is(JANUARY));
		assertThat(interval.getEnd().getMonthOfYear(), is(DECEMBER));
		//
		assertThat(interval.getStart().getDayOfMonth(), is(1));
		assertThat(interval.getEnd().getDayOfMonth(), is(31));
		//
		assertThat(interval.getStart().getHourOfDay(), is(0));
		assertThat(interval.getEnd().getHourOfDay(), is(23));
	}

	@Test
	public void getIntervalThisMonth() {
		Interval interval = Period.THIS_MONTH.getInterval();
		assertThat(interval.getStart().getYear(), is(now.getYear()));
		assertThat(interval.getEnd().getYear(), is(now.getYear()));
		//
		assertThat(interval.getStart().getMonthOfYear(), is(now.getMonthOfYear()));
		assertThat(interval.getEnd().getMonthOfYear(), is(now.getMonthOfYear()));
		//
		assertThat(interval.getStart().getDayOfMonth(), is(1));
		assertThat(interval.getEnd().getDayOfMonth(), is(now.dayOfMonth().getMaximumValue()));
		//
		assertThat(interval.getStart().getHourOfDay(), is(0));
		assertThat(interval.getEnd().getHourOfDay(), is(23));
	}

	@Test
	public void getIntervalThisWeekMidYear() {
		now = now.withMonthOfYear(JULY);
		//
		Interval interval = Period.THIS_WEEK.getInterval(now);
		//
		assertThat(interval.getStart().getYear(), is(now.getYear()));
		assertThat(interval.getEnd().getYear(), is(now.getYear()));
		//
		assertThat(interval.getStart().getDayOfMonth(), is(now.minusDays(now.getDayOfWeek() - 1).getDayOfMonth()));
		assertThat(interval.getEnd().getDayOfMonth(), is(now.plusDays(7 - now.getDayOfWeek()).getDayOfMonth()));
		//
		assertThat(interval.getStart().getDayOfWeek(), is(MONDAY));
		assertThat(interval.getEnd().getDayOfWeek(), is(SUNDAY));
		//
		assertThat(interval.getStart().getHourOfDay(), is(0));
		assertThat(interval.getEnd().getHourOfDay(), is(23));
	}

	@Test
	public void getIntervalThisWeek_StartsPreviousYear() {
		now = now.withYear(2016).withMonthOfYear(JANUARY).withDayOfMonth(2);
		//
		Interval interval = Period.THIS_WEEK.getInterval(now);
		//
		assertThat(interval.getStart().getYear(), is(2015));
		assertThat(interval.getEnd().getYear(), is(now.getYear()));
		//
		assertThat(interval.getStart().getMonthOfYear(), is(DECEMBER));
		assertThat(interval.getEnd().getMonthOfYear(), is(JANUARY));
		//
		assertThat(interval.getStart().getDayOfMonth(), is(28));
		assertThat(interval.getEnd().getDayOfMonth(), is(3));
		//
		assertThat(interval.getStart().getDayOfWeek(), is(MONDAY));
		assertThat(interval.getEnd().getDayOfWeek(), is(SUNDAY));
		//
		assertThat(interval.getStart().getHourOfDay(), is(0));
		assertThat(interval.getEnd().getHourOfDay(), is(23));
	}

	@Test
	public void getIntervalLastYear() {
		Interval interval = Period.LAST_YEAR.getInterval();
		assertThat(interval.getStart().getYear(), is(now.getYear() - 1));
		assertThat(interval.getEnd().getYear(), is(now.getYear() - 1));
		//
		assertThat(interval.getStart().getMonthOfYear(), is(JANUARY));
		assertThat(interval.getEnd().getMonthOfYear(), is(DECEMBER));
		//
		assertThat(interval.getStart().getDayOfMonth(), is(1));
		assertThat(interval.getEnd().getDayOfMonth(), is(31));
		//
		assertThat(interval.getStart().getHourOfDay(), is(0));
		assertThat(interval.getEnd().getHourOfDay(), is(23));
	}

	@Test
	public void getIntervalLastMonthFebruary() {
		now = now.withMonthOfYear(FEBRUARY);
		//
		Interval interval = Period.LAST_MONTH.getInterval(now);
		//
		assertThat(interval.getStart().getYear(), is(now.getYear()));
		assertThat(interval.getEnd().getYear(), is(now.getYear()));
		//
		assertThat(interval.getStart().getMonthOfYear(), is(JANUARY));
		assertThat(interval.getEnd().getMonthOfYear(), is(JANUARY));
		//
		assertThat(interval.getStart().getDayOfMonth(), is(1));
		assertThat(interval.getEnd().getDayOfMonth(), is(31));
		//
		assertThat(interval.getStart().getHourOfDay(), is(0));
		assertThat(interval.getEnd().getHourOfDay(), is(23));
	}

	@Test
	public void getIntervalLastMonthJanuary() {
		now = now.withMonthOfYear(JANUARY);
		//
		Interval interval = Period.LAST_MONTH.getInterval(now);
		//
		assertThat(interval.getStart().getYear(), is(now.getYear() - 1));
		assertThat(interval.getEnd().getYear(), is(now.getYear() - 1));
		//
		assertThat(interval.getStart().getMonthOfYear(), is(DECEMBER));
		assertThat(interval.getEnd().getMonthOfYear(), is(DECEMBER));
		//
		assertThat(interval.getStart().getDayOfMonth(), is(1));
		assertThat(interval.getEnd().getDayOfMonth(), is(31));
		//
		assertThat(interval.getStart().getHourOfDay(), is(0));
		assertThat(interval.getEnd().getHourOfDay(), is(23));
	}

	@Test
	public void getIntervalLastMonthApril() {
		now = now.withMonthOfYear(APRIL).withDayOfMonth(1);
		//
		Interval interval = Period.LAST_MONTH.getInterval(now);
		//
		assertThat(interval.getStart().getYear(), is(now.getYear()));
		assertThat(interval.getEnd().getYear(), is(now.getYear()));
		//
		assertThat(interval.getStart().getMonthOfYear(), is(MARCH));
		assertThat(interval.getEnd().getMonthOfYear(), is(MARCH));
		//
		assertThat(interval.getStart().getDayOfMonth(), is(1));
		assertThat(interval.getEnd().getDayOfMonth(), is(31));
		//
		assertThat(interval.getStart().getHourOfDay(), is(0));
		assertThat(interval.getEnd().getHourOfDay(), is(23));
	}

	@Test
	public void getIntervalLastMonthMarch() {
		now = now.withMonthOfYear(MARCH).withDayOfMonth(1);
		//
		Interval interval = Period.LAST_MONTH.getInterval(now);
		//
		assertThat(interval.getStart().getYear(), is(now.getYear()));
		assertThat(interval.getEnd().getYear(), is(now.getYear()));
		//
		assertThat(interval.getStart().getMonthOfYear(), is(FEBRUARY));
		assertThat(interval.getEnd().getMonthOfYear(), is(FEBRUARY));
		//
		assertThat(interval.getStart().getDayOfMonth(), is(1));

		assertThat(Long.valueOf(interval.getEnd().getDayOfMonth()),
				is(new DateTime().withMonthOfYear(FEBRUARY).monthOfYear().toInterval().toDuration().getStandardDays()));
		//
		assertThat(interval.getStart().getHourOfDay(), is(0));
		assertThat(interval.getEnd().getHourOfDay(), is(23));
	}

	@Test
	public void getIntervalLastWeek_WeekStartLastYearEndsThisYear() {
		now = now.withYear(2016).withMonthOfYear(JANUARY).withDayOfMonth(5);
		//
		Interval interval = Period.LAST_WEEK.getInterval(now);
		//
		assertThat(interval.getStart().getYear(), is(2015));
		assertThat(interval.getEnd().getYear(), is(2016));
		//
		assertThat(interval.getStart().getDayOfMonth(), is(28));
		assertThat(interval.getEnd().getDayOfMonth(), is(3));
		//
		assertThat(interval.getStart().getMonthOfYear(), is(DECEMBER));
		assertThat(interval.getEnd().getMonthOfYear(), is(JANUARY));
		//
		//
		assertThat(interval.getStart().getDayOfWeek(), is(MONDAY));
		assertThat(interval.getEnd().getDayOfWeek(), is(SUNDAY));
		//
		assertThat(interval.getStart().getHourOfDay(), is(0));
		assertThat(interval.getEnd().getHourOfDay(), is(23));
	}

	@Test
	public void getIntervalLastWeek_MidYear() {
		now = now.withMonthOfYear(JULY);
		//
		Interval interval = Period.LAST_WEEK.getInterval(now);
		//
		assertThat(interval.getStart().getYear(), is(now.getYear()));
		assertThat(interval.getEnd().getYear(), is(now.getYear()));
		//
		assertThat(interval.getStart().getDayOfMonth(),
				is(now.minusWeeks(1).minusDays(now.getDayOfWeek() - 1).getDayOfMonth()));
		assertThat(interval.getEnd().getDayOfMonth(),
				is(now.minusWeeks(1).plusDays(7 - now.getDayOfWeek()).getDayOfMonth()));
		//
		assertThat(interval.getStart().getDayOfWeek(), is(MONDAY));
		assertThat(interval.getEnd().getDayOfWeek(), is(SUNDAY));
		//
		assertThat(interval.getStart().getHourOfDay(), is(0));
		assertThat(interval.getEnd().getHourOfDay(), is(23));
	}
}
