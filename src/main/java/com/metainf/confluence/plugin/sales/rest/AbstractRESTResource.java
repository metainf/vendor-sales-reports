package com.metainf.confluence.plugin.sales.rest;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by TiborHegyi on 2019. 03. 14..
 */
abstract public class AbstractRESTResource {

	protected Response unlicensed() {
		return Response.status(Response.Status.UNAUTHORIZED).entity("Email This Issue License is invalid").cacheControl(CacheControls.NO_CACHE).build();
	}
	protected Response ok() {
		return Response.ok().cacheControl(CacheControls.NO_CACHE).build();
	}

	protected Response ok(Object entity) {
		return Response.ok(entity).cacheControl(CacheControls.NO_CACHE).build();
	}

	protected Response status(int statusNumber, String message) {
		return Response.status(statusNumber).entity(message).cacheControl(CacheControls.NO_CACHE).build();
	}

	protected Response serverError(String error) {
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(error).cacheControl(CacheControls.NO_CACHE).build();
	}

	protected Response serverError(Throwable t) {
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(t.getMessage()).cacheControl(CacheControls.NO_CACHE).build();
	}

	protected Response error(String error) {
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorModel(error)).cacheControl(CacheControls.NO_CACHE).build();
	}

	protected Response error(Throwable t) {
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ErrorModel(t.getMessage())).cacheControl(CacheControls.NO_CACHE).build();
	}

}
