/**
 *
 */
package com.metainf.confluence.plugin.sales.rest;

import javax.ws.rs.core.CacheControl;

/**
 * @author Tibor Hegyi
 */
public class CacheControls extends CacheControl {
  public final static CacheControl NO_CACHE = new CacheControl();

  static {
    NO_CACHE.setNoStore(true);
    NO_CACHE.setNoCache(true);
  }
}//

