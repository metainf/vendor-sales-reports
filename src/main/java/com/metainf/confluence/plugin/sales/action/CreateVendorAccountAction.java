/*
 * #%L
 * Vendor Sales Reports
 * %%
 * Copyright (C) 2014 - 2016 META-INF KFT
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 * #L%
 */
package com.metainf.confluence.plugin.sales.action;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.user.GroupManager;
import com.metainf.atlassian.sales.data.VendorAccount;
import com.metainf.atlassian.sales.data.VendorAccountDao;
import org.apache.commons.lang.StringUtils;

/**
 * Created with IntelliJ IDEA.
 * User: tibor
 * Date: 2012.12.29.
 * Time: 14:30
 * To change this template use File | Settings | File Templates.
 */
@SuppressWarnings("serial")
public class CreateVendorAccountAction extends ConfigurePluginAction {

  private Long vendorAccountId;
  private Long newVendorId;
  private String newVendorName;
  private String newVendorEmail;
  private String newVendorPassword;
  private String notificationGroup;
  private String customerViewGroup;
  private String notificationType;

  public CreateVendorAccountAction(VendorAccountDao vendorAccountDao, ApplicationProperties applicationProperties, GroupManager groupManager) {
    super(vendorAccountDao, applicationProperties, groupManager);
  }

  @Override
  public String execute() throws Exception {
    if (StringUtils.isNotBlank(this.newVendorEmail) && this.newVendorId != null && this.newVendorId > 0) {
      try {
        if (this.vendorAccountId != null) {
          VendorAccount account = this.vendorAccountDao.getById(this.vendorAccountId);
          account.setEmailAddress(this.newVendorEmail);
          account.setVendorId( this.newVendorId);
          if (StringUtils.isNotBlank(this.newVendorPassword)) {
            account.setPassword(EncryptUtil.encrypt(this.newVendorPassword));
          }
          account.setVendorName(this.newVendorName);
          account.setNotificationGroup(this.notificationGroup);
          account.setNotificationType(this.notificationType);
          account.setCustomerViewGroup(customerViewGroup);
          this.vendorAccountDao.update(account);
        } else {
          this.vendorAccountDao.create(this.newVendorEmail, EncryptUtil.encrypt(this.newVendorPassword), this.newVendorId, this.newVendorName, this.notificationGroup, this.notificationType, this.customerViewGroup);
        }

      } catch (Exception e) {

      }
    }
    return super.execute();
  }

  public Long getVendorAccountId() {
    return vendorAccountId;
  }

  public void setVendorAccountId(Long vendorAccountId) {
    this.vendorAccountId = vendorAccountId;
  }

  public Long getNewVendorId() {
    return newVendorId;
  }

  public void setNewVendorId(Long newVendorId) {
    this.newVendorId = newVendorId;
  }

  public String getNewVendorName() {
    return newVendorName;
  }

  public void setNewVendorName(String newVendorName) {
    this.newVendorName = newVendorName;
  }

  public String getNewVendorEmail() {
    return newVendorEmail;
  }

  public void setNewVendorEmail(String newVendorEmail) {
    this.newVendorEmail = newVendorEmail;
  }

  public String getNewVendorPassword() {
    return newVendorPassword;
  }

  public void setNewVendorPassword(String newVendorPassword) {
    this.newVendorPassword = newVendorPassword;
  }

  public String getNotificationGroup() {
    return notificationGroup;
  }

  public void setNotificationGroup(String notificationGroup) {
    this.notificationGroup = notificationGroup;
  }

  public String getNotificationType() {
    return notificationType;
  }

  public void setNotificationType(String notificationType) {
    this.notificationType = notificationType;
  }

  public String getCustomerViewGroup() {
    return customerViewGroup;
  }

  public void setCustomerViewGroup(String customerViewGroup) {
    this.customerViewGroup = customerViewGroup;
  }
}//

