/*
 * #%L
 * Vendor Sales Reports
 * %%
 * Copyright (C) 2014 - 2016 META-INF KFT
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 * #L%
 */
package com.metainf.confluence.plugin.sales.macro;

import com.metainf.atlassian.sales.data.SaleItem;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: tibor
 * Date: 2012.12.29.
 * Time: 9:34
 * To change this template use File | Settings | File Templates.
 */
public class SalesAggregator {
  private AggregateType aggregateType;
  private Map<String, AggregatedItem> aggregatedResult = new HashMap<String, AggregatedItem>();

  public SalesAggregator(AggregateType aggregateType) {
    this.aggregateType = aggregateType;
  } 

  public void add(SaleItem sale) {
    String key = this.aggregateType.calculateKey(sale);
    AggregatedItem item = aggregatedResult.containsKey(key) ? aggregatedResult.get(key) : new AggregatedItem(key);
    item.addSale(sale);
    this.aggregatedResult.put(key, item);
  } //

  public List<AggregatedItem> aggregate(SaleItem[] sales) {
    for (SaleItem item : sales) {
      add(item);
    }//

    return new ArrayList<AggregatedItem>(this.aggregatedResult.values());
  } //

  public Map<String, AggregatedItem> getAggregatedResult() {
    return aggregatedResult;
  }

  public AggregatedItem aggregateTotal(List<AggregatedItem> aggregatedItems) {
    AggregatedItem total = new AggregatedItem(null);

    if (aggregatedItems != null) {
      for (SalesAggregator.AggregatedItem aggregatedItem : aggregatedItems) {
        total.totalSalesPrice += aggregatedItem.getTotalSalesPrice();
        total.totalVendorAmount += aggregatedItem.getTotalVendorAmount();
        total.totalVendorNewSale += aggregatedItem.getTotalVendorNewSale();
        total.totalVendorRenewal += aggregatedItem.getTotalVendorRenewal();
        total.totalVendorUpgrade += aggregatedItem.getTotalVendorUpgrade();
        
        // FIXME: This code does not work properly in all cases as it's comparing strings which represend dates
        // in most cases
        if (total.dateOfFirstSale == null || total.dateOfFirstSale.compareTo(aggregatedItem.getPurchaseDate()) > 0) {
          total.dateOfFirstSale = aggregatedItem.getPurchaseDate();
        }

        if (total.dateOfLastSale == null || total.dateOfLastSale.compareTo(aggregatedItem.getPurchaseDate()) < 0) {
          total.dateOfLastSale = aggregatedItem.getPurchaseDate();
        }
      }
    }

    return total;
  }

  public static class AggregatedItem implements Comparable<AggregatedItem> {
    private String purchaseDate;
    private long sortBy;
    private int count;
    private double totalSalesPrice = 0;
    private double totalVendorAmount = 0;
    private double totalVendorNewSale = 0;
    private double totalVendorRenewal = 0;
    private double totalVendorUpgrade = 0;
    private String dateOfFirstSale = null;
    private String dateOfLastSale = null;
    private NumberFormat percentFormat = NumberFormat.getPercentInstance(Locale.US);

    public AggregatedItem(String purchaseDate) {
      this.purchaseDate = purchaseDate;
      this.percentFormat.setMaximumFractionDigits(0);
    }

    public String getDateOfFirstSale() {
      return dateOfFirstSale;
    }

    public String getDateOfLastSale() {
      return dateOfLastSale;
    }

    public String getPurchaseDate() {
      return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
      this.purchaseDate = purchaseDate;
    }

    public int getCount() {
      return count;
    }


    public Double getTotalSalesPrice() {
    	return totalSalesPrice;
    }
    
    public Double getTotalVendorAmount() {
    	return totalVendorAmount;
    }
    
    public Double getTotalVendorNewSale() {
    	return totalVendorNewSale;
    }
    
    public Double getTotalVendorRenewal() {
    	return totalVendorRenewal;
    }
    
    public Double getTotalVendorUpgrade() {
    	return totalVendorUpgrade;
    }

    public void addSale(SaleItem sale) {
      this.count++;
      this.sortBy = sale.getDateOfSale() != null ? sale.getDateOfSale().getTime() : this.purchaseDate.hashCode();
      this.totalSalesPrice += sale.getSalesPrice();
      this.totalVendorAmount += sale.getVendorAmount();
      switch(sale.getSalesType().toLowerCase()) {
    	  case "new":
    		  this.totalVendorNewSale += sale.getVendorAmount();
    		  break;
    	  case "renewal":
    		  this.totalVendorRenewal += sale.getVendorAmount();
    		  break;
    	  case "upgrade":
    		  this.totalVendorUpgrade += sale.getVendorAmount();
    		  break;
    	  case "refund":
    		  //TODO: Probably the code should check the original type of the refunded record and substract from the proper bucket
    		  // use addition (+) as the amount will be negative number
    		  this.totalVendorNewSale +=  sale.getVendorAmount();
    		  this.count -= 2;
    		  break;
      }
      
    }

    public String newSaleShare() {
      return calculateShare(this.totalVendorNewSale);
    }

    public String renewShare() {
      return calculateShare(this.totalVendorRenewal);
    }

    public String upgradeShare() {
      return calculateShare(this.totalVendorUpgrade);
    }

    public String calculateShare(double base) {
      if (this.totalVendorAmount == 0) {
        return percentFormat.format(0);
      } else {
        return this.percentFormat.format(base / this.totalVendorAmount);
      }
    }

    public String calculateShareOfTotal(double totalVendorSales) {
      if (totalVendorSales == 0) {
        return percentFormat.format(0);
      } else {
        return this.percentFormat.format(this.totalVendorAmount / totalVendorSales);
      }
    }

    @Override
    public int compareTo(AggregatedItem o) {
      //return this.purchaseDate.compareTo(o.getPurchaseDate());
      long diff = this.sortBy - o.sortBy;
      return diff == 0 ? 0 : (diff > 0 ? 1 : -1);
    }
  } //
  public static enum AggregateType {
    DETAILED("yyyy-MM-dd HH:mm:ss"), BY_DAY("yyyy-MM-dd"), BY_WEEK("yyyy/w"), BY_MONTH("yyyy MMM"), BY_YEAR("yyyy"), TOTAL(""),
    BY_ADDON("") {
      @Override
      public String calculateKey(SaleItem saleItem) {
        String pluginName = saleItem.getPluginName();

        //plugin name translation only for META-INF add-ons
        pluginName = "JIRA Email This Issue Plugin".equalsIgnoreCase(pluginName) ? "Email This Issue" : pluginName;
        pluginName = "Bug Watcher".equalsIgnoreCase(pluginName) ? "Bug Watcher Notifications" : pluginName;

        return pluginName;
      }
    },
    BY_SALETYPE("") {
      @Override
      public String calculateKey(SaleItem saleItem) {
        return saleItem.getSalesType();
      }
    },
    BY_COUNTRY("") {
      @Override
      public String calculateKey(SaleItem saleItem) {
        return saleItem.getCountry();
      }
    },

    BY_LICENSE_TYPE(""){
      @Override
      public String calculateKey(SaleItem saleItem) {
        return saleItem.getLicenseType();
      }
    },
    BY_LICENSE_SIZE(""){
      @Override
      public String calculateKey(SaleItem saleItem) {
        return saleItem.getLicenseSize();
      }
    },
    BY_HOSTING("") {
      @Override
      public String calculateKey(SaleItem saleItem) {
        return saleItem.getHosting();
      }
    },
    BY_EXPERT("") {
      @Override
      public String calculateKey(SaleItem saleItem) {
        return saleItem.getExpertName();
      }
    }
    ;

    private static Map<String, AggregateType> types = new HashMap<String, AggregateType>(5);
    static {
      types.put(SalesMacro.OUTPUT_DETAILED, DETAILED);
      types.put(SalesMacro.OUTPUT_AGGREGATEBYDAY, BY_DAY);
      types.put(SalesMacro.OUTPUT_AGGREGATEBYWEEK, BY_WEEK);
      types.put(SalesMacro.OUTPUT_AGGREGATEBYMONTH, BY_MONTH);
      types.put(SalesMacro.OUTPUT_AGGREGATEBYYEAR, BY_YEAR);
      types.put(SalesMacro.OUTPUT_AGGREGATEBYADDON, BY_ADDON);
      types.put(SalesMacro.OUTPUT_AGGREGATEBYSALETYPE, BY_SALETYPE);
      types.put(SalesMacro.OUTPUT_AGGREGATEBYCOUNTRY, BY_COUNTRY);
      types.put(SalesMacro.OUTPUT_TOTAL, TOTAL);
      types.put(SalesMacro.OUTPUT_AGGREGATEBYLICENSETYPE, BY_LICENSE_TYPE);
      types.put(SalesMacro.OUTPUT_AGGREGATEBYLICENSESIZE, BY_LICENSE_SIZE);
      types.put(SalesMacro.OUTPUT_AGGREGATEBYHOSTING, BY_HOSTING);
      types.put(SalesMacro.OUTPUT_AGGREGATEBYEXPERT, BY_EXPERT);
    }

    private String name;

    private AggregateType(String name) {
      this.name = name;
    }

    public String calculateKey(SaleItem saleItem) {
      Date date = saleItem.getDateOfSale();
      return date == null || "".equalsIgnoreCase(this.name) ? "Total" : new SimpleDateFormat(this.name).format(date);
    }

    public static AggregateType getByTypeValue(String type) {
      return types.get(type);
    }
  } //
}
