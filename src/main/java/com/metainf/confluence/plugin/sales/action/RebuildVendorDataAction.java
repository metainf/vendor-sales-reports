/*
 * #%L
 * Vendor Sales Reports
 * %%
 * Copyright (C) 2014 - 2016 META-INF KFT
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 * #L%
 */
package com.metainf.confluence.plugin.sales.action;

import com.atlassian.confluence.schedule.ScheduledJobKey;
import com.atlassian.confluence.schedule.managers.ScheduledJobManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.user.GroupManager;
import com.metainf.atlassian.sales.data.VendorAccountDao;

/**
 * Created with IntelliJ IDEA. User: tibor Date: 2012.12.29. Time: 14:30 To
 * change this template use File | Settings | File Templates.
 */
@SuppressWarnings("serial")
public class RebuildVendorDataAction extends ConfigurePluginAction {

	private ScheduledJobManager scheduledJobManager;

	public RebuildVendorDataAction(VendorAccountDao vendorAccountDao, ApplicationProperties applicationProperties,
			GroupManager groupManager) {
		super(vendorAccountDao, applicationProperties, groupManager);

	}

	public void setScheduledJobManager(final ScheduledJobManager scheduledJobManager) {
		this.scheduledJobManager = scheduledJobManager;
	}

	@Override
	public String execute() throws Exception {
		scheduledJobManager.runNow(
				new ScheduledJobKey("com.metainf.confluence.plugin.vendor-sales-reports","vendor-sales-rebuilder"));
		return super.execute();
	}

}//
