package com.metainf.confluence.plugin.sales.rest;

import com.metainf.atlassian.sales.data.SaleItem;
import com.metainf.confluence.plugin.sales.macro.SalesDataBrowserMacro;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Locale;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class SaleItemModel {
	private String expertName;
	private String invoice;
	private String dateOfSale;
	private String licenseId;
	private String pluginKey;
	private String pluginName;
	private String organization;
	private String technicalContactName;
	private String technicalContactEmailAddress;
	private String billingContactName;
	private String billingContactEmailAddress;
	private String country;
	private String licenseSize;
	private String licenseType;
	private String salesType;
	private String salesPrice;
	private String vendorAmount;
	private String maintenanceStart;
	private String maintenanceEnd;

	private String hosting;
	private String billingPeriod;
	private String addonLicenseId;
	private String hostLicenseId;
	private String region;
	private String lastUpdated;
	private String status;
	private String statusClassName;
	private String appEntitlementNumber;

	public SaleItemModel() {
	}
	public SaleItemModel(SaleItem saleItem, boolean authorizedToViewPricing) {
		this.expertName = saleItem.getExpertName();
		this.invoice = saleItem.getInvoice();
		this.dateOfSale = saleItem.getDateOfSale() != null ? DateFormat.getDateInstance().format(saleItem.getDateOfSale()) : null;
		this.licenseId = saleItem.getLicenseId();
		this.pluginKey = saleItem.getPluginKey();
		this.pluginName = saleItem.getPluginName();
		this.organization = saleItem.getOrganisation();
		this.technicalContactName = saleItem.getTechnicalContactName();
		this.technicalContactEmailAddress = saleItem.getTechnicalContactEmail();
		this.billingContactName = saleItem.getBillingContactName();
		this.billingContactEmailAddress = saleItem.getBillingContactEmail();
		this.country = saleItem.getCountry();
		this.licenseSize = saleItem.getLicenseSize();
		this.licenseType = saleItem.getLicenseType();
		this.salesType = saleItem.getSalesType();
		this.maintenanceStart = saleItem.getMaintenanceStart();
		this.maintenanceEnd = saleItem.getMaintenanceEnd();

		this.hosting = saleItem.getHosting();
		this.billingPeriod = saleItem.getBillingPeriod();
		this.addonLicenseId = saleItem.getAddonLicenseId();
		this.hostLicenseId = saleItem.getHostLicenseId();
		this.region = saleItem.getRegion();
		this.lastUpdated = saleItem.getLastUpdated() != null ? DateFormat.getDateInstance().format(saleItem.getLastUpdated()) : null;
		this.appEntitlementNumber = saleItem.getAppEntitlementNumber();

		if (authorizedToViewPricing) {
			this.salesPrice = NumberFormat.getCurrencyInstance(Locale.US).format(saleItem.getSalesPrice());
			this.vendorAmount = NumberFormat.getCurrencyInstance(Locale.US).format(saleItem.getVendorAmount());
		}

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate maintenanceEnds = this.maintenanceEnd != null ? LocalDate.parse(this.maintenanceEnd, dtf) : null;


		if (maintenanceEnds == null) {
			this.status = "Unknown";
			this.statusClassName="";
		} else {
			LocalDate now = LocalDate.now();
			long dayDiff = ChronoUnit.DAYS.between(maintenanceEnds, now);

			if (dayDiff <= -30) {
				this.status="Active";
				this.statusClassName="aui-lozenge-success";
			} else if (dayDiff < 0) {
				this.status = "Expires soon";
				this.statusClassName="aui-lozenge-moved";
			} else {
				this.status = "Expired";
				this.statusClassName = "aui-lozenge-error";
			}
		}
	}

	public String getExpertName() {
		return expertName;
	}

	public void setExpertName(String expertName) {
		this.expertName = expertName;
	}

	public String getInvoice() {
		return invoice;
	}

	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}

	public String getDateOfSale() {
		return dateOfSale;
	}

	public void setDateOfSale(String dateOfSale) {
		this.dateOfSale = dateOfSale;
	}

	public String getLicenseId() {
		return licenseId;
	}

	public void setLicenseId(String licenseId) {
		this.licenseId = licenseId;
	}

	public String getPluginKey() {
		return pluginKey;
	}

	public void setPluginKey(String pluginKey) {
		this.pluginKey = pluginKey;
	}

	public String getPluginName() {
		return pluginName;
	}

	public void setPluginName(String pluginName) {
		this.pluginName = pluginName;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getTechnicalContactName() {
		return technicalContactName;
	}

	public void setTechnicalContactName(String technicalContactName) {
		this.technicalContactName = technicalContactName;
	}

	public String getTechnicalContactEmailAddress() {
		return technicalContactEmailAddress;
	}

	public void setTechnicalContactEmailAddress(String technicalContactEmailAddress) {
		this.technicalContactEmailAddress = technicalContactEmailAddress;
	}

	public String getBillingContactName() {
		return billingContactName;
	}

	public void setBillingContactName(String billingContactName) {
		this.billingContactName = billingContactName;
	}

	public String getBillingContactEmailAddress() {
		return billingContactEmailAddress;
	}

	public void setBillingContactEmailAddress(String billingContactEmailAddress) {
		this.billingContactEmailAddress = billingContactEmailAddress;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getLicenseSize() {
		return licenseSize;
	}

	public void setLicenseSize(String licenseSize) {
		this.licenseSize = licenseSize;
	}

	public String getLicenseType() {
		return licenseType;
	}

	public void setLicenseType(String licenseType) {
		this.licenseType = licenseType;
	}

	public String getSalesType() {
		return salesType;
	}

	public void setSalesType(String salesType) {
		this.salesType = salesType;
	}

	public String getSalesPrice() {
		return salesPrice;
	}

	public void setSalesPrice(String salesPrice) {
		this.salesPrice = salesPrice;
	}

	public String getVendorAmount() {
		return vendorAmount;
	}

	public void setVendorAmount(String vendorAmount) {
		this.vendorAmount = vendorAmount;
	}

	public String getMaintenanceStart() {
		return maintenanceStart;
	}

	public void setMaintenanceStart(String maintenanceStart) {
		this.maintenanceStart = maintenanceStart;
	}

	public String getMaintenanceEnd() {
		return maintenanceEnd;
	}

	public void setMaintenanceEnd(String maintenanceEnd) {
		this.maintenanceEnd = maintenanceEnd;
	}

	public String getHosting() {
		return hosting;
	}

	public void setHosting(String hosting) {
		this.hosting = hosting;
	}

	public String getBillingPeriod() {
		return billingPeriod;
	}

	public void setBillingPeriod(String billingPeriod) {
		this.billingPeriod = billingPeriod;
	}

	public String getAddonLicenseId() {
		return addonLicenseId;
	}

	public void setAddonLicenseId(String addonLicenseId) {
		this.addonLicenseId = addonLicenseId;
	}

	public String getHostLicenseId() {
		return hostLicenseId;
	}

	public void setHostLicenseId(String hostLicenseId) {
		this.hostLicenseId = hostLicenseId;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getStatus() {
		return status;
	}

	public String getStatusClassName() {
		return statusClassName;
	}

	public String getAppEntitlementNumber() {
		return appEntitlementNumber;
	}

	public void setAppEntitlementNumber(String appEntitlementNumber) {
		this.appEntitlementNumber = appEntitlementNumber;
	}
}
