/*
 * #%L
 * Vendor Sales Reports
 * %%
 * Copyright (C) 2014 - 2016 META-INF KFT
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 * #L%
 */
package com.metainf.confluence.plugin.sales.action;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.user.EntityException;
import com.atlassian.user.Group;
import com.atlassian.user.GroupManager;
import com.atlassian.user.search.page.Pager;
import com.metainf.atlassian.sales.data.VendorAccount;
import com.metainf.atlassian.sales.data.VendorAccountDao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: tibor
 * Date: 2012.12.29.
 * Time: 13:59
 * To change this template use File | Settings | File Templates.
 */
@SuppressWarnings("serial")
public class ConfigurePluginAction extends ConfluenceActionSupport {
  protected VendorAccountDao vendorAccountDao;
  private ApplicationProperties applicationProperties;
  private GroupManager groupManager;

  public ConfigurePluginAction(VendorAccountDao vendorAccountDao, ApplicationProperties applicationProperties, GroupManager groupManager) {
    this.vendorAccountDao = vendorAccountDao;
    this.applicationProperties = applicationProperties;
    this.groupManager = groupManager;

  }

  public List<Group> getGroups() {
    List<Group> groups = new ArrayList<Group>();
    try {
      Pager<Group> pages = this.groupManager.getGroups();
      while(!pages.isEmpty()) {
        groups.addAll(pages.getCurrentPage());
        if (pages.onLastPage()) {
          break;
        } else {
          pages.nextPage();
        }
      }

    } catch (EntityException e) {
    }//

    return groups;
  }

  public String getBaseUrl() {
    return applicationProperties.getBaseUrl(UrlMode.CANONICAL);
  }
  public List<VendorAccount> getVendorAccounts() {
    return new ArrayList<VendorAccount>(Arrays.asList(this.vendorAccountDao.findAll()));
  }
}//
