/*
 * #%L
 * Vendor Sales Reports
 * %%
 * Copyright (C) 2014 - 2016 META-INF KFT
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 * #L%
 */
package com.metainf.atlassian.sales.job;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.atlassian.confluence.mail.template.ConfluenceMailQueueItem;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.core.util.DateUtils;
import com.atlassian.mywork.model.NotificationBuilder;
import com.atlassian.mywork.service.LocalNotificationService;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.user.Group;
import com.atlassian.user.search.page.Pager;
import com.metainf.atlassian.sales.ReportGenerator;
import com.metainf.atlassian.sales.ReportResult;
import com.metainf.atlassian.sales.data.SaleItem;
import com.metainf.atlassian.sales.data.SaleItemDao;
import com.metainf.atlassian.sales.data.VendorAccount;
import com.metainf.atlassian.sales.data.VendorAccountDao;
import com.metainf.atlassian.sales.mail.MailService;
import com.metainf.confluence.plugin.sales.macro.SalesAggregator;

/**
 * Created with IntelliJ IDEA.
 * User: tibor
 * Date: 2012.12.27.
 * Time: 6:25
 * To change this template use File | Settings | File Templates.
 */
public class VendorSalesDownloaderJob extends AbstractDownloader implements Job {
  private static transient Logger logger = Logger.getLogger(VendorSalesDownloaderJob.class);
  private static final String PLUGIN_KEY = "com.metainf.confluence.plugin.vendor-sales-reports";

  private final SaleItemDao saleItemDao;
  private final VendorAccountDao vendorAccountDao;
  private final MailService mailService;
  private TransactionTemplate transactionTemplate;
  private LocalNotificationService notificationService;
  private UserAccessor userAccessor;

  public VendorSalesDownloaderJob(SaleItemDao saleItemDao, VendorAccountDao vendorAccountDao, TransactionTemplate transactionTemplate, final LocalNotificationService localNotificationService, UserAccessor userAccessor, MailService mailService) {
    this.saleItemDao = saleItemDao;
    this.vendorAccountDao = vendorAccountDao;
    this.notificationService = localNotificationService;
    this.transactionTemplate = transactionTemplate;
    this.userAccessor = userAccessor;
    this.mailService = mailService;
  }


  @Override
  public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
    try {
      final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      this.transactionTemplate.execute(new TransactionCallback<Object>() {
        @Override
        public Object doInTransaction() {

          VendorAccount[] accounts = VendorSalesDownloaderJob.this.vendorAccountDao.findAll();

          for (VendorAccount vendor : accounts) {
            try {

              Date fetchDate = new Date();

              logger.info("Downloading sales for "+vendor.getVendorName());

              SaleItem lastItem = VendorSalesDownloaderJob.this.saleItemDao.findLastItem(vendor);
              logger.info("Last item: "+lastItem);

              Date startDate = lastItem != null ? lastItem.getDateOfSale() : (vendor.getLastFetch() == null ? DateUtils.getDateDay(2001, 1, 1) : vendor.getLastFetch());
              logger.info("Start date: "+startDate+", last fetch: "+vendor.getLastFetch());

              ReportResult rr = new ReportGenerator().startDate(startDate).endDate(null).vendorAccount(vendor).generate();

              StringBuilder emailNotification = new StringBuilder();

              // Build an index of existing items and use it to skip adding already existing items
              Set<String> itemIds = getExistingItemsIds(VendorSalesDownloaderJob.this.saleItemDao.findAll());

              for (ReportResult.SalesItem item : rr.getSalesItems()) {
                if (!itemIds.contains(getSaleItemId(item))) {
                  //we have not yet persisted this sales item
                  SaleItem newSale = VendorSalesDownloaderJob.this.saleItemDao.create(vendor, item);
                  // Update the lookup index
                  itemIds.add(getSaleItemId(item));

                  String notificationText = sendNotification(vendor,  newSale);
                  if (StringUtils.isNotBlank(notificationText)) {
                    emailNotification.append(emailNotification.length() > 0 ? "\n\n" : "").append(notificationText);
                  }
                  logger.debug("sale item created for invoice: "+newSale.getInvoice()+", entitlement: "+newSale.getAppEntitlementNumber());
                }
              }
              sendEmail(vendor, emailNotification);

              vendor.setLastFetch(fetchDate);
              vendor.setResultOfLastFetch("SUCCESS");

            } catch (Exception e) {
              logger.error(e.getMessage(), e);
              String m = e.getMessage();
              vendor.setResultOfLastFetch(m.substring(0, Math.min(450, m.length())));
            }//try

            VendorSalesDownloaderJob.this.vendorAccountDao.update(vendor);
          }//

          return null;  //To change body of implemented methods use File | Settings | File Templates.
        }
      })  ;

      logger.debug("Vendor Sales Job executed");
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }

  }//

  private void sendEmail(VendorAccount vendor, StringBuilder emailNotification) {
    try {
      if (StringUtils.isNotBlank(vendor.getNotificationGroup()) && emailNotification.length() > 0 && ("email".equalsIgnoreCase(vendor.getNotificationType()) || "all".equalsIgnoreCase(vendor.getNotificationType()))) {
        Group notificationGroup = this.userAccessor.getGroup(vendor.getNotificationGroup());
        if (notificationGroup != null) {

          //Iterable<ConfluenceUser> members = this.userAccessor.getMembers(notificationGroup);
          Pager<String> members = this.userAccessor.getMemberNames(notificationGroup);
          //if (!members.isEmpty()) {
          if (members != null) {

            for (String member : members) {
              if (member != null) {
                logger.info("About to send email to: " + member);
                ConfluenceUser memberUser = this.userAccessor.getUserByName(member);
                ConfluenceMailQueueItem mailQueueItem = new ConfluenceMailQueueItem(memberUser.getEmail(),"New sale(s) of your add-ons", emailNotification.toString(), "text/plain");
                this.mailService.sendEmail(mailQueueItem);
              }
            } //for
          }//
        }//
      }//

    } catch (Exception e) {
      logger.warn(e.getMessage(),  e);
    }
  }

  private String sendNotification(VendorAccount vendor, SaleItem newSale) {
    String notificationText = "";
    try {
      if (StringUtils.isNotBlank(vendor.getNotificationGroup())) {
        Group notificationGroup = this.userAccessor.getGroup(vendor.getNotificationGroup());
        if (notificationGroup != null) {

          //Iterable<ConfluenceUser> members = this.userAccessor.getMembers(notificationGroup);
          Pager<String> members = this.userAccessor.getMemberNames(notificationGroup);
          //if (!members.isEmpty()) {
          if (members != null) {

            String thisMonthTotal = null;
            try {
              SaleItem[] hits = this.saleItemDao.thisMonth(null);//newSale.getPluginName());
              SalesAggregator aggregator = new SalesAggregator(SalesAggregator.AggregateType.BY_MONTH);
              SalesAggregator.AggregatedItem totalMonth = aggregator.aggregateTotal(aggregator.aggregate(hits));
              thisMonthTotal = String.valueOf(totalMonth.getTotalVendorAmount());
            } catch (Exception e) {
              logger.warn(e.getMessage(), e);
              thisMonthTotal = e.getMessage();
            }

            StringBuilder sb = new StringBuilder();
            sb.append("Add-on: ").append(newSale.getPluginName()).append("\n");
            sb.append("Organisation: ").append(newSale.getOrganisation()).append("\n");
            sb.append("Country: ").append(newSale.getCountry()).append("\n");
            sb.append("Sale Type: ").append(newSale.getSalesType()).append("\n");
            sb.append("Size: ").append(newSale.getLicenseSize()).append("\n");
            sb.append("Purchase Price: $").append(newSale.getSalesPrice()).append("\n");
            sb.append("Vendor Amount: $").append(newSale.getVendorAmount()).append("\n").append("\n");
            sb.append("This Month's Total (all Add-ons): $").append(thisMonthTotal).append("\n");

            notificationText = sb.toString();

            if (StringUtils.isNotBlank(vendor.getNotificationType()) && ("inapp".equalsIgnoreCase(vendor.getNotificationType()) || "all".equalsIgnoreCase(vendor.getNotificationType()))) {
              for (String member : members) {
                if (member != null) {
                  notificationService.createOrUpdate(member, new NotificationBuilder()
                          .application(PLUGIN_KEY) // a unique key that identifies your plugin
                          .itemTitle("New sale(s) of your add-on " + newSale.getPluginName() + "'")
                          .title("Your add-on '" + newSale.getPluginName() + "' has been purchased")
                          .description(sb.toString())
                          .groupingId(PLUGIN_KEY + "." + newSale.getPluginName()) // a key to aggregate notifications
                          .createNotification());
                }
              } //for
            }
          }//
        }//
      }//

    } catch (Exception e) {
      logger.warn(e.getMessage(),  e);
    }

    return notificationText;
  } //

} //
