/*
 * #%L
 * Vendor Sales Reports
 * %%
 * Copyright (C) 2014 - 2016 META-INF KFT
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 * #L%
 */
package com.metainf.atlassian.sales.job;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.metainf.atlassian.sales.ReportResult.SalesItem;
import com.metainf.atlassian.sales.data.SaleItem;

public abstract class AbstractDownloader {

	  protected Set<String> getExistingItemsIds(SaleItem[] items) {
		  	List<String> itemIdsList = Lists.transform(Arrays.asList(items), new Function<SaleItem, String>() {
				@Override
				public String apply(SaleItem input) {
					return getSaleItemId(input);
				}
            });
		  
			return Sets.newHashSet(itemIdsList);
	  }
	  
	  protected String getSaleItemId(SaleItem input) {
			return input.getInvoice()+"_"+input.getLicenseId()+"_"+input.getSalesType();
	  }
	  protected String getSaleItemId(SalesItem input) {
		  return input.getInvoice()+"_"+input.getLicenseId()+"_"+input.getSalesType();
	  }
}
