/*
 * #%L
 * Vendor Sales Reports
 * %%
 * Copyright (C) 2014 - 2016 META-INF KFT
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 * #L%
 */
package com.metainf.atlassian.sales.job;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.metainf.atlassian.sales.ReportGenerator;
import com.metainf.atlassian.sales.ReportResult;
import com.metainf.atlassian.sales.data.SaleItem;
import com.metainf.atlassian.sales.data.SaleItemDao;
import com.metainf.atlassian.sales.data.VendorAccount;
import com.metainf.atlassian.sales.data.VendorAccountDao;

/**
 * Created with Eclipse.
 * User: borgeorgiev
 * Date: 2016.01.03.
 * Time: 23:30
 * To change this template use File | Settings | File Templates.
 */
public class VendorSalesRebuilderJob extends AbstractDownloader implements Job {
  private static transient Logger logger = Logger.getLogger(VendorSalesRebuilderJob.class);

  private final SaleItemDao saleItemDao;
  private final VendorAccountDao vendorAccountDao;
  private TransactionTemplate transactionTemplate;

  public VendorSalesRebuilderJob(SaleItemDao saleItemDao, VendorAccountDao vendorAccountDao, TransactionTemplate transactionTemplate) {
    this.saleItemDao = saleItemDao;
    this.vendorAccountDao = vendorAccountDao;
    this.transactionTemplate = transactionTemplate;
  }

  @Override
  public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
    try {
      this.transactionTemplate.execute(new TransactionCallback<Object>() {
        @Override
        public Object doInTransaction() {

          VendorAccount[] accounts = VendorSalesRebuilderJob.this.vendorAccountDao.findAll();

          for (VendorAccount vendor : accounts) {
            try {

              Date fetchDate = new Date();
              //Delete all items
              VendorSalesRebuilderJob.this.saleItemDao.deleteAll();
              
              // Fetch all available on the marketplace
              ReportResult rr = new ReportGenerator().vendorAccount(vendor).generateAll();
              
              Set<String> itemIds = new HashSet<>();
              for (ReportResult.SalesItem item : rr.getSalesItems()) {
				  if (itemIds.contains(getSaleItemId(item))) {
					 continue;
				  }
	              //we have not yet persisted this sales item
	              SaleItem newSale = VendorSalesRebuilderJob.this.saleItemDao.create(vendor, item);
	              itemIds.add(getSaleItemId(newSale));
	              
	              logger.debug("sale item created for invoice: "+newSale.getInvoice());
              }//

              vendor.setLastFetch(fetchDate);
              vendor.setResultOfLastFetch("SUCCESS");

            } catch (Exception e) {
              logger.error(e.getMessage(), e);
              vendor.setResultOfLastFetch(e.getMessage());
            }//try

            VendorSalesRebuilderJob.this.vendorAccountDao.update(vendor);
          }//

          return null;  //To change body of implemented methods use File | Settings | File Templates.
        }
      })  ;

      logger.debug("Vendor Rebuilder Job executed");
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
  }
  
}
