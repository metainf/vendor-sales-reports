/*
 * #%L
 * Vendor Sales Reports
 * %%
 * Copyright (C) 2014 - 2016 META-INF KFT
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 * #L%
 */
package com.metainf.atlassian.sales.mail;

import com.atlassian.core.task.MultiQueueTaskManager;
import com.atlassian.mail.queue.MailQueueItem;

/**
 * Default implementation of the {@link MailService}
 */
public class MailServiceImpl implements MailService {
  public static final String MAIL = "mail";
  private final MultiQueueTaskManager taskManager;

  public MailServiceImpl(MultiQueueTaskManager multiQueueTaskManager) {
    this.taskManager = multiQueueTaskManager;
  }

  /**
   * This will use a MultiQueueTaskManager to add add the mailQueueItem to a queue
   * to be sent
   *
   * @param mailQueueItem the item to send
   */
  @Override
  public void sendEmail(MailQueueItem mailQueueItem) {
    taskManager.addTask(MAIL, mailQueueItem);
  }
}