/*
 * #%L
 * Vendor Sales Reports
 * %%
 * Copyright (C) 2014 - 2016 META-INF KFT
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 * #L%
 */
package com.metainf.atlassian.sales.mail;

import com.atlassian.mail.queue.MailQueueItem;


/**
 * This service has the responsibility of sending an email
 */
public interface MailService {
  /**
   * This will send an email based on the details stored in the ConfluenceMailQueueItem
   *
   * @param mailQueueItem the item to send
   */
  void sendEmail(MailQueueItem mailQueueItem);
}