/*
 * #%L
 * Vendor Sales Reports
 * %%
 * Copyright (C) 2014 - 2016 META-INF KFT
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 * #L%
 */
package com.metainf.atlassian.sales.data;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.metainf.atlassian.sales.Params;
import com.metainf.atlassian.sales.Period;
import com.metainf.atlassian.sales.ReportResult;
import static com.metainf.atlassian.sales.Params.*;
import net.java.ao.Query;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.Interval;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: tibor
 * Date: 2012.12.27.
 * Time: 6:20
 * To change this template use File | Settings | File Templates.
 */
public class SaleItemDao extends GenericDao<SaleItem> {
  
  private static final String PERIOD_ALL = "All".toUpperCase();
  private static final String PERIOD_THISMONTH = "This month".toUpperCase();

  public SaleItemDao(ActiveObjects ao) {
    super(ao);
  }

  public SaleItem create(final VendorAccount vendor, final ReportResult.SalesItem item) {
    return this.ao.executeInTransaction(new TransactionCallback<SaleItem>() {
      @Override
      public SaleItem doInTransaction() {
        SaleItem t = SaleItemDao.this.ao.create(getPersistentClass());
        t.setVendorAccount(vendor);
        //t.setVendorAccountId(vendor.getID());
        item.updateEntity(t);
        t.save();
        return t;
      }
    });
  }

  public SaleItem findLastItem(VendorAccount vendorAccount) {
    SaleItem[] hits = this.ao.find(SaleItem.class, Query.select().where("VENDOR_ACCOUNT_ID = ? AND VENDOR_AMOUNT > 0", vendorAccount.getID()).order("ID DESC").limit(1));
    return hits == null || hits.length == 0 ? null : hits[0];
  } //
  @Override
  protected Class<SaleItem> getPersistentClass() {
    return SaleItem.class;
  }

  public SaleItem[] thisMonth(String addOn) {
    Map<String, String> parameters = new HashMap<String, String>();
    parameters.put(PARAM_REPORTPERIOD, PERIOD_THISMONTH);
    if (StringUtils.isNotBlank(addOn)) {
      parameters.put(PARAM_ADDONNAME, addOn);
    }//
    return doSearch(parameters);
  }

  public SaleItem[] doSearch(Map<String, String> parameters) {
    String addOnName = parameters.get(PARAM_ADDONNAME);
    String startDateString = parameters.get(PARAM_STARTDATE);
    String endDateString = parameters.get(PARAM_ENDDATE);
    String period = parameters.containsKey(PARAM_REPORTPERIOD) ? parameters.get(PARAM_REPORTPERIOD) : PERIOD_ALL;
    String sortDirection = parameters.get(Params.PARAM_SORTDIRECTION);
    SortDirection sort = SortDirection.ASC;
    if(!StringUtils.isEmpty(sortDirection)){
    	sort = SortDirection.valueOf(sortDirection);
    }
    Interval interval = Period.fromParam(period).getInterval();
    DateTime startDate = new DateTime(getDateIfInCorrectFormat(startDateString, "2000-01-01"));
    DateTime endDate = new DateTime(getDateIfInCorrectFormat(endDateString, "2100-01-01"));
    
    //here we know all date variables are not null
    //we take the narrowest period if both start/end date and period defined
    DateTime periodStart = interval.getStart();
    DateTime periodEnd = interval.getEnd();
    periodStart = startDate != null && startDate.isAfter(interval.getStart()) ? startDate : periodStart;
    periodEnd = endDate != null && endDate.isBefore(periodEnd) ? endDate : periodEnd;

	SaleItem[] hits = StringUtils.isBlank(addOnName) ?
            filter("DATE_OF_SALE >= ? AND DATE_OF_SALE <= ?",sort, periodStart.toDate(), periodEnd.toDate()) :
            filter("DATE_OF_SALE >= ? AND DATE_OF_SALE <= ? AND PLUGIN_NAME = ?",sort, periodStart.toDate(), periodEnd.toDate(), addOnName);;
    return hits;
  }

  private Date getDateIfInCorrectFormat(String dateString, String defaultValue) {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    try {
      return sdf.parse(StringUtils.isBlank(dateString) ? defaultValue : dateString);
    } catch (ParseException e) {
      try {
        return sdf.parse(defaultValue);
      } catch (ParseException e1) {
        return new Date();
      }
    }
  }//

	

}//
