/*
 * #%L
 * Vendor Sales Reports
 * %%
 * Copyright (C) 2014 - 2016 META-INF KFT
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 * #L%
 */
package com.metainf.atlassian.sales.data;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.StringLength;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: tibor
 * Date: 2012.12.27.
 * Time: 9:58
 * To change this template use File | Settings | File Templates.
 */
@Preload
public interface VendorAccount extends Entity {
  String getEmailAddress();
  void setEmailAddress(String emailAddress);
  String getPassword();
  void setPassword(String password);
  String getVendorName();
  void setVendorName(String vendorName);
  Long getVendorId();
  void setVendorId(Long vendorId);

  @StringLength(256)
  String getNotificationGroup();
  void setNotificationGroup(String notificationGroup);

  @StringLength(256)
  String getCustomerViewGroup();
  void setCustomerViewGroup(String customerViewGroup);

  String getNotificationType();
  void setNotificationType(String notificationType);

  Date getLastFetch();
  void setLastFetch(Date lastFetch);

  Date getLastStartDate();
  void setLastStartDate(Date lastStartDate);

  Integer getLastOffset();
  void setLastOffset(Integer offset);

  @StringLength(255)
  String getResultOfLastFetch();
  void setResultOfLastFetch(String resultOfLastFetch);
}
