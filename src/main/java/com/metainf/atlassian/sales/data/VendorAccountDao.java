/*
 * #%L
 * Vendor Sales Reports
 * %%
 * Copyright (C) 2014 - 2016 META-INF KFT
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 * #L%
 */
package com.metainf.atlassian.sales.data;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.sal.api.transaction.TransactionCallback;

/**
 * Created with IntelliJ IDEA.
 * User: tibor
 * Date: 2012.12.27.
 * Time: 10:22
 * To change this template use File | Settings | File Templates.
 */
public class VendorAccountDao extends GenericDao<VendorAccount> {
  private SaleItemDao saleItemDao;

  public VendorAccountDao(ActiveObjects ao, SaleItemDao saleItemDao) {
    super(ao);
    this.saleItemDao = saleItemDao;
  }

  @Override
  protected Class<VendorAccount> getPersistentClass() {
    return VendorAccount.class;
  }

  public void delete(final VendorAccount account) {
    if (account != null) {
    this.ao.executeInTransaction(new TransactionCallback<VendorAccount>() {
      @Override
      public VendorAccount doInTransaction() {
        //VendorAccountDao.this.saleItemDao.delete("VENDOR_ACCOUNT = ?", account);
        SaleItem[] sales = VendorAccountDao.this.saleItemDao.filter("VENDOR_ACCOUNT_ID = ?", account.getID());
        VendorAccountDao.this.ao.delete(sales);
        VendorAccountDao.this.ao.delete(account);
        return account;
      }
    });
    }//
  }

  public VendorAccount create(final String email, final String password, final Long vendorId, final String name, final String notificationGroup, final String notificationType, final String customerViewGroup) {
    return this.ao.executeInTransaction(new TransactionCallback<VendorAccount>() {
      @Override
      public VendorAccount doInTransaction() {
        VendorAccount account = VendorAccountDao.this.ao.create(getPersistentClass());
        account.setEmailAddress(email);
        account.setVendorId(vendorId);
        account.setPassword(password);
        account.setVendorName(name);
        account.setNotificationGroup(notificationGroup);
        account.setNotificationType(notificationType);
        account.setCustomerViewGroup(customerViewGroup);
        account.save();
        return account;
      }
    });
  }
}//
