/*
 * #%L
 * Vendor Sales Reports
 * %%
 * Copyright (C) 2014 - 2016 META-INF KFT
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 * #L%
 */
package com.metainf.atlassian.sales.data;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.sal.api.transaction.TransactionCallback;
import net.java.ao.Entity;
import net.java.ao.Query;

import org.apache.log4j.Logger;

import java.util.Arrays;

abstract public class GenericDao<T extends Entity> {
protected ActiveObjects ao;
  protected Logger logger = Logger.getLogger(getClass());

  public GenericDao(ActiveObjects ao) {
    this.ao = ao;
  }

  public T[] filter(String where,  Object... params) {
	  return filter(where, SortDirection.ASC, params);
  }
  public T[] filter(String where, SortDirection sort, Object... params) {
    return GenericDao.this.ao.find(getPersistentClass(), Query.select().where(where, params).order("DATE_OF_SALE "+sort.toString()));
  };

  public T create() {
    return this.ao.executeInTransaction(new TransactionCallback<T>() {
      @Override
      public T doInTransaction() {
        T t = GenericDao.this.ao.create(getPersistentClass());
        t.save();
        return t;
      }
    });
  }//

  public T getById(Long id) {
    if (id == null) return null;

    T[] hits = this.ao.find(getPersistentClass(), Query.select().where("ID = ?", id));
    return hits == null || hits.length == 0 ? null : hits[0];
  }

  public void delete(final T entity) {
    if (entity != null) {
      this.ao.executeInTransaction(new TransactionCallback<T>() {
        @Override
        public T doInTransaction() {
          GenericDao.this.ao.delete(entity);
          return entity;
        }
      });
    }//if
  }//delete

  public T[] findAll(String orderBy) {
    return ao.find(getPersistentClass(), Query.select().order(orderBy));
  }

  public T[] findAll() {
    return ao.find(getPersistentClass());
  }

  public void deleteAll() {
    final T[] hits = findAll();
    for (int i = 0; i * 50 < hits.length; i++) {
      final T[] chunk = Arrays.copyOfRange(hits, i * 50, Math.min((i + 1) * 50, hits.length));
      ao.executeInTransaction(new TransactionCallback<T>() {
        @Override
        public T doInTransaction() {
          GenericDao.this.ao.delete(chunk);
          return null;
        }
      });
    }
  }
  public void delete(final String where, final Object... param) {
    this.ao.executeInTransaction(new TransactionCallback<T>() {
      @Override
      public T doInTransaction() {
        T[] hits = GenericDao.this.filter(where, param);
        GenericDao.this.ao.delete(hits);
        return null;
      }//
    });
  }//

  public void update(final T pw) {
    this.ao.executeInTransaction(new TransactionCallback<T>() {
      @Override
      public T doInTransaction() {
        pw.save();
        return pw;
      }//
    });
  }//

  public T[] findLimited(int limit)
  {
    return ao.find(getPersistentClass(), Query.select().limit(limit));
  }

  abstract protected Class<T> getPersistentClass();


}//class
