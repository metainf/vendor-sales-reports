/*
 * #%L
 * Vendor Sales Reports
 * %%
 * Copyright (C) 2014 - 2016 META-INF KFT
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 * #L%
 */
package com.metainf.atlassian.sales;

import com.metainf.atlassian.sales.data.VendorAccount;

import com.metainf.confluence.plugin.sales.action.EncryptUtil;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.methods.*;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by IntelliJ IDEA.
 * User: HegyiT
 * Date: 2012.11.22.
 * Time: 13:27
 * To change this template use File | Settings | File Templates.
 */
public class ReportGenerator {
  private static final Logger logger = Logger.getLogger(ReportGenerator.class);
	final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	private static int LIMIT=50;
  private Date startDate;
  private Date endDate;
  private VendorAccount vendorAccount;

  public ReportGenerator() {
  }

  public ReportGenerator startDate(Date startDate) {
    this.startDate = startDate;
    return this;
  }

  public ReportGenerator endDate(Date endDate) {
    this.endDate = endDate;
    return this;
  }

  public ReportGenerator vendorAccount(VendorAccount vendorAccount) {
    this.vendorAccount = vendorAccount;
    return this;
  }

  public ReportResult generate() throws Exception {
    ReportResult result = new ReportResult();

    int offset = this.vendorAccount.getLastOffset() == null ? 0 : this.vendorAccount.getLastOffset();

		if (this.vendorAccount.getLastStartDate() == null || !this.vendorAccount.getLastStartDate().equals(startDate)) {
			offset = 0;
			this.vendorAccount.setLastOffset(offset);
			//last start date changed, keep offset to 0
			this.vendorAccount.setLastStartDate(startDate);
		}

    JSONArray sales = grabSales(startDate, endDate, offset, LIMIT);
    if (sales != null && sales.length() > 0) {
			//increment offset
			this.vendorAccount.setLastOffset(offset + sales.length());

      for (int i = 0; i < sales.length(); i++) {
        JSONObject item = sales.getJSONObject(i);
        ReportResult.SalesItem salesItem = toSalesItem(item);
        result.addSalesItem(salesItem);
        logger.info("Sale item for invoice: "+salesItem.getInvoice()+", "+salesItem.getAppEntitlementNumber());
      }

    }

    return result;
  }

  public ReportResult generateAll() throws Exception {
	  ReportResult result = new ReportResult();
	  final String sdf = "yyyy-MM-dd";

		Calendar start = Calendar.getInstance();
		start.set(2000, Calendar.JANUARY, 1, 0, 0, 0);

		Calendar end = Calendar.getInstance();
		end.add(Calendar.DAY_OF_YEAR, 1);

	  int offset = 0;
	  while(true) {
			JSONArray sales = grabSales(start.getTime(), end.getTime(), offset, 50);
			if(sales == null || sales.length() == 0) {
				break;
			}

			// Process returned sales
			if (sales != null) {
					for (int i = 0; i < sales.length(); i++) {
						JSONObject item = sales.getJSONObject(i);
						ReportResult.SalesItem salesItem = toSalesItem(item);
						result.addSalesItem(salesItem);
					}
				}
			// Get Next page
			// get in overlapping chunks as the marketplace API seems to have some bug which results in some missing records
			offset+=40;
	  }

	  return result;
  }

  //this method supports API 2
	private ReportResult.SalesItem toSalesItem(JSONObject item) {
		ReportResult.SalesItem salesItem = new ReportResult.SalesItem();

		//process level 1 attributes
		salesItem.setInvoice(item.optString("transactionId"));
		salesItem.setAddonLicenseId(item.optString("addonLicenseId"));
		salesItem.setHostLicenseId(item.optString("hostLicenseId"));
		salesItem.setLicenseId(item.optString("licenseId"));
		salesItem.setPluginName(item.optString("addonName"));
		salesItem.setPluginKey(item.optString("addonKey"));
		salesItem.setLastUpdated(item.optString("lastUpdated"));
		salesItem.setAppEntitlementNumber(item.optString("appEntitlementNumber"));

		//process customerDetails
		JSONObject customerDetails = item.optJSONObject("customerDetails");
		if (customerDetails != null) {
			salesItem.setOrganisation(customerDetails.optString("company"));
			salesItem.setCountry(customerDetails.optString("country"));
			salesItem.setRegion(customerDetails.optString("region"));

			JSONObject contact = customerDetails.optJSONObject("technicalContact");
			if (contact != null) {
				salesItem.setTechnicalContactName(contact.optString("name"));
				salesItem.setTechnicalContactEmail(contact.optString("email"));
			}

			contact = customerDetails.optJSONObject("billingContact");
			if (contact != null) {
				salesItem.setBillingContactName(contact.optString("name"));
				salesItem.setBillingContactEmail(contact.optString("email"));
			}
		}

		//process purchaseDetails
		JSONObject purchaseDetails = item.optJSONObject("purchaseDetails");
		if (purchaseDetails != null) {
			salesItem.setDate(purchaseDetails.optString("saleDate"));
			salesItem.setLicenseSize(purchaseDetails.optString("tier"));
			salesItem.setLicenseType(purchaseDetails.optString("licenseType"));
			salesItem.setHosting(purchaseDetails.optString("hosting", "Server"));
			salesItem.setBillingPeriod(purchaseDetails.optString("billingPeriod"));
			salesItem.setSalesPrice(purchaseDetails.optDouble("purchasePrice"));
			salesItem.setVendorAmount(purchaseDetails.optDouble("vendorAmount"));
			salesItem.setSalesType(purchaseDetails.optString("saleType"));
			salesItem.setMaintenanceStart(purchaseDetails.optString("maintenanceStartDate"));
			salesItem.setMaintenanceEnd(purchaseDetails.optString("maintenanceEndDate"));
		}

		//process partner details
		JSONObject partnerDetails = item.optJSONObject("partnerDetails");
		if (partnerDetails != null) {
			salesItem.setExpertName(partnerDetails.optString("partnerName"));

			JSONObject contact = partnerDetails.optJSONObject("billingContact");
			if (contact != null) {
				salesItem.setBillingContactName(contact.optString("name"));
				salesItem.setBillingContactEmail(contact.optString("email"));
			}

		} else {
			salesItem.setExpertName("Direct sale");
		}


		return salesItem;
	}

private ReportResult.SalesItem toSalesItemAPI1(JSONObject item) {
    ReportResult.SalesItem salesItem = new ReportResult.SalesItem();
    salesItem.setCountry(item.optString("country"));

    JSONObject contact = item.optJSONObject("technicalContact");
    if (contact != null) {
      salesItem.setTechnicalContactName(contact.optString("name"));
      salesItem.setTechnicalContactEmail(contact.optString("email"));
    }

    contact = item.optJSONObject("billingContact");
    if (contact != null) {
      salesItem.setBillingContactName(contact.optString("name"));
      salesItem.setBillingContactEmail(contact.optString("email"));
    }

    salesItem.setDate(item.optString("date"));
    salesItem.setInvoice(item.optString("invoice"));
    salesItem.setOrganisation(item.optString("organisationName"));
    salesItem.setLicenseId(item.optString("licenseId"));
    salesItem.setPluginName(item.optString("pluginName"));

    salesItem.setLicenseSize(item.optString("licenseSize"));
    salesItem.setLicenseType(item.optString("licenseType"));
    salesItem.setSalesType(item.optString("saleType"));

    salesItem.setSalesPrice(item.optDouble("purchasePrice"));
    salesItem.setVendorAmount(item.optDouble("vendorAmount"));

    salesItem.setMaintenanceStart(item.optString("maintenanceStartDate"));
    salesItem.setMaintenanceEnd(item.optString("maintenanceEndDate"));
    salesItem.setExpertName(item.optString("expertName"));

    return salesItem;
  }

  private JSONArray grabSales(Date start, Date end, int offset,int limit) throws Exception {
	  HttpClient httpclient = new HttpClient();
	  //String url = "https://marketplace.atlassian.com/rest/1.0/vendors/" + vendorAccount.getVendorId() + "/sales?sort-by=date&limit="+limit+"&offset="+offset;
		String url = "https://marketplace.atlassian.com/rest/2/vendors/" + vendorAccount.getVendorId() + "/reporting/sales/transactions?order=asc&sortBy=date&limit="+limit+"&offset="+offset;
	  if (end != null) {
		  url += "&endDate=" + this.sdf.format(end);
	  }
	  if (start != null) {
		  url += "&startDate=" + this.sdf.format(start);
	  }
	  GetMethod get = new GetMethod(url);

	  logger.info("executing request: " + url);

	  String basic_auth = new String(Base64.encodeBase64((vendorAccount.getEmailAddress() + ":" + EncryptUtil.decrypt(vendorAccount.getPassword())).getBytes()));
	  get.addRequestHeader("Authorization", "Basic " + basic_auth);

	  JSONArray sales = null;
		try {
			// Execute the method.
			int statusCode = httpclient.executeMethod(get);

			logger.info("Response: "+statusCode + ", " + get.getStatusLine());

			if (statusCode != HttpStatus.SC_OK) {
				throw new Exception(get.getStatusLine().toString());
			}

			// Read the response body.
			byte[] responseBody = get.getResponseBody();
			String responseText = new String(responseBody);
			logger.info("Response content length: " + responseText.length());
			JSONObject json = new JSONObject(responseText);

			logger.info("Transaction data: "+json.has("transactions"));
			sales = json.getJSONArray("transactions");

			logger.info("Sales data: "+sales.length());

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			// Release the connection.
			get.releaseConnection();
		}

	  return sales;
  }



}
