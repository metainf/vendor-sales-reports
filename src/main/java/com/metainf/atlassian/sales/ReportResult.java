/*
 * #%L
 * Vendor Sales Reports
 * %%
 * Copyright (C) 2014 - 2016 META-INF KFT
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 * #L%
 */
package com.metainf.atlassian.sales;

import com.metainf.atlassian.sales.data.SaleItem;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: HegyiT
 * Date: 2012.11.22.
 * Time: 13:29
 * To change this template use File | Settings | File Templates.
 */
public class ReportResult {
  private List<SalesItem> salesItems = new ArrayList<SalesItem>();

  public List<SalesItem> getSalesItems() {
    return salesItems;
  }

  public void toWriter(Writer writer) {
    try {
      writer.write(toString());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void addSalesItem(SalesItem salesItem) {
    this.salesItems.add(salesItem);
  }

  public static class SalesItem {
    private String invoice;
    private String date;
    private String licenseId;
    private String pluginKey;
    private String pluginName;
    private String organisation;
    private String technicalContactName;
    private String technicalContactEmail;
    private String billingContactName;
    private String billingContactEmail;
    private String country;
    private String licenseSize;
    private String licenseType;
    private String salesType;
    private double salesPrice;
    private double vendorAmount;
    private String expertName;
    private String maintenanceStart;
    private String maintenanceEnd;

    private String hosting;
    private String billingPeriod;
    private String addonLicenseId;
    private String hostLicenseId;

    private String region;
    private String lastUpdated;
    private String appEntitlementNumber;

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public String getExpertName() {
      return expertName;
    }


    public void setExpertName(String expertName) {
      this.expertName = expertName;
    }


    public String getInvoice() {
      return invoice;
    }


    public void setInvoice(String invoice) {
      this.invoice = invoice;
    }

    public Date getDateOfSale() {
      try {
        return StringUtils.isBlank(this.date) ? null : this.sdf.parse(this.date);
      } catch (ParseException e) {
        return null;
      }
    }
    public String getDate() {
      return date;
    }


    public void setDate(String date) {
      this.date = date;
    }


    public String getLicenseId() {
      return licenseId;
    }


    public void setLicenseId(String licenseId) {
      this.licenseId = licenseId;
    }


    public String getPluginName() {
      return pluginName;
    }


    public void setPluginName(String pluginName) {
      this.pluginName = pluginName;
    }


    public String getOrganisation() {
      return organisation;
    }


    public void setOrganisation(String organisation) {
      this.organisation = organisation;
    }


    public String getTechnicalContactName() {
      return technicalContactName;
    }


    public void setTechnicalContactName(String technicalContactName) {
      this.technicalContactName = technicalContactName;
    }


    public String getTechnicalContactEmail() {
      return technicalContactEmail;
    }


    public void setTechnicalContactEmail(String technicalContactEmail) {
      this.technicalContactEmail = technicalContactEmail;
    }


    public String getBillingContactName() {
      return billingContactName;
    }


    public void setBillingContactName(String billingContactName) {
      this.billingContactName = billingContactName;
    }


    public String getBillingContactEmail() {
      return billingContactEmail;
    }


    public void setBillingContactEmail(String billingContactEmail) {
      this.billingContactEmail = billingContactEmail;
    }


    public String getCountry() {
      return country;
    }


    public void setCountry(String country) {
      this.country = country;
    }


    public String getLicenseSize() {
      return licenseSize;
    }


    public void setLicenseSize(String licenseSize) {
      this.licenseSize = licenseSize;
    }


    public String getLicenseType() {
      return licenseType;
    }


    public void setLicenseType(String licenseType) {
      this.licenseType = licenseType;
    }


    public String getSalesType() {
      return salesType;
    }


    public void setSalesType(String salesType) {
      this.salesType = salesType;
    }


    public double getSalesPrice() {
      return salesPrice;
    }


    public void setSalesPrice(double salesPrice) {
      this.salesPrice = salesPrice;
    }


    public double getVendorAmount() {
      return vendorAmount;
    }


    public void setVendorAmount(double vendorAmount) {
      this.vendorAmount = vendorAmount;
    }


    public String getMaintenanceStart() {
      return maintenanceStart;
    }


    public void setMaintenanceStart(String maintenanceStart) {
      this.maintenanceStart = maintenanceStart;
    }


    public String getMaintenanceEnd() {
      return maintenanceEnd;
    }


    public void setMaintenanceEnd(String maintenanceEnd) {
      this.maintenanceEnd = maintenanceEnd;
    }

    public String getPluginKey() {
      return pluginKey;
    }

    public void setPluginKey(String pluginKey) {
      this.pluginKey = pluginKey;
    }

    public String getHosting() {
      return hosting;
    }

    public void setHosting(String hosting) {
      this.hosting = hosting;
    }

    public String getBillingPeriod() {
      return billingPeriod;
    }

    public void setBillingPeriod(String billingPeriod) {
      this.billingPeriod = billingPeriod;
    }

    public String getAddonLicenseId() {
      return addonLicenseId;
    }

    public void setAddonLicenseId(String addonLicenseId) {
      this.addonLicenseId = addonLicenseId;
    }

    public String getHostLicenseId() {
      return hostLicenseId;
    }

    public void setHostLicenseId(String hostLicenseId) {
      this.hostLicenseId = hostLicenseId;
    }

    public String getRegion() {
      return region;
    }

    public void setRegion(String region) {
      this.region = region;
    }

    public String getLastUpdated() {
      return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
      this.lastUpdated = lastUpdated;
    }

    public Date getLastUpdatedDate() {
      try {
        return StringUtils.isBlank(this.lastUpdated) ? null : this.sdf.parse(this.lastUpdated);
      } catch (ParseException e) {
        return null;
      }
    }

    public String getAppEntitlementNumber() {
      return appEntitlementNumber;
    }

    public void setAppEntitlementNumber(String appEntitlementNumber) {
      this.appEntitlementNumber = appEntitlementNumber;
    }

    public void updateEntity(SaleItem t) {
      t.setBillingContactEmail(getBillingContactEmail());
      t.setBillingContactName(getBillingContactName());
      t.setCountry(getCountry());
      t.setDateOfSale(getDateOfSale());
      t.setInvoice(getInvoice());
      t.setLicenseId(getLicenseId());
      t.setPluginKey(getPluginKey());
      t.setPluginName(getPluginName());
      t.setOrganisation(getOrganisation());
      t.setTechnicalContactEmail(getTechnicalContactEmail());
      t.setTechnicalContactName(getTechnicalContactName());
      t.setLicenseSize(getLicenseSize());
      t.setLicenseType(getLicenseType());
      t.setSalesPrice(getSalesPrice());
      t.setVendorAmount(getVendorAmount());
      t.setSalesType(getSalesType());
      t.setExpertName(getExpertName());
      t.setMaintenanceEnd(getMaintenanceEnd());
      t.setMaintenanceStart(getMaintenanceStart());

      t.setHosting(getHosting());
      t.setBillingPeriod(getBillingPeriod());
      t.setAddonLicenseId(getAddonLicenseId());
      t.setHostLicenseId(getHostLicenseId());

      t.setRegion(getRegion());
      t.setLastUpdated(getLastUpdatedDate());

      t.setAppEntitlementNumber(getAppEntitlementNumber());
    }
  }
}
