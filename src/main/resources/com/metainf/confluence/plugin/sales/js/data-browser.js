$(function() {
    AJS.VENDOR_SALES = {
        search: function(term) {
            console.log("searching for sales data");
            AJS.$.ajax({
                url: AJS.contextPath() + "/rest/vendorsales/1.0/data?term=" + term,
                type: "GET",
                cache: false,
                dataType: "json",
                success: function(data) {
                    console.log("Data search result:");console.log(data);
                    var $dataTable = AJS.$("#vendor-sales-browser");
                    if (data.length == 0) {
                        $dataTable.hide();
                    } else {
                        $dataTable.show();
                    }
                    var $tbody = $dataTable.find("tbody");

                    if ($tbody) {
                        $tbody.find("tr").remove();
                        data.forEach(function (value, index, array) {
                            var $tr = AJS.$("<tr></tr>");
                            $tr.append(AJS.$("<td class='confluenceTd'></td>").append(AJS.$("<span class='status-macro aui-lozenge conf-macro output-inline' data-hasbody='false' data-macro-name='status'>").addClass(value.statusClassName).append(value.status)));
                            $tr.append(AJS.$("<td class='confluenceTd'></td>").append(value.dateOfSale));
                            $tr.append(AJS.$("<td class='confluenceTd'></td>").append(value.invoice));
                            $tr.append(AJS.$("<td class='confluenceTd'></td>").append(value.licenseId));
                            $tr.append(AJS.$("<td class='confluenceTd'></td>").append(value.appEntitlementNumber));
                            $tr.append(AJS.$("<td class='confluenceTd'></td>").append(value.pluginName));
                            $tr.append(AJS.$("<td class='confluenceTd'></td>").append(value.hosting));
                            $tr.append(AJS.$("<td class='confluenceTd'></td>").append(value.billingPeriod));
                            $tr.append(AJS.$("<td class='confluenceTd'></td>").append(value.maintenanceEnd));
                            $tr.append(AJS.$("<td class='confluenceTd'></td>").append(value.organization));
                            $tr.append(AJS.$("<td class='confluenceTd'></td>").append(value.country));
                            $tr.append(AJS.$("<td class='confluenceTd'></td>").append(value.region));
                            $tr.append(AJS.$("<td class='confluenceTd'></td>").append(value.technicalContactName + " ("+value.technicalContactEmailAddress+")"));
                            $tr.append(AJS.$("<td class='confluenceTd'></td>").append(value.billingContactName + " ("+value.billingContactEmailAddress+")"));
                            $tr.append(AJS.$("<td class='confluenceTd'></td>").append(value.licenseSize));
                            $tr.append(AJS.$("<td class='confluenceTd'></td>").append(value.licenseType));
                            $tr.append(AJS.$("<td class='confluenceTd'></td>").append(value.salesType));
                            $tr.append(AJS.$("<td class='confluenceTd'></td>").append(value.expertName));

                            if ($dataTable.find("#data-price").length > 0) {
                                $tr.append(AJS.$("<td class='confluenceTd'></td>").append(value.salesPrice));
                                $tr.append(AJS.$("<td class='confluenceTd'></td>").append(value.vendorAmount));
                            }

                            $tbody.append($tr);
                        });
                    }
                },
                error: function() {
                    console.log("Data search failed")
                }
            });
        }
    }
    console.log("vendor sales javascript loaded")
});
