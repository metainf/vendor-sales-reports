# Vendor Sales Reports

Vendor Sales Reports may be used by vendors who sell plugins via the Atlassian Marketplace.

It contains two major components and features:

* A scheduled job that fetches sale records via the Atlassian Sales API and stores them in your Confluence database
* A macro that can dispay sale records in a tabular fashion. The macro output may be used to generate charts using Confluence Charts.
* In-app notifications on new sales to not miss every single transaction
* Support multiple vendor accounts to aggregate more into a report 

## Using the Plugin

Please consult our [Tutorial Page](http://www.meta-inf.hu/wiki/display/PLUG/Vendor%20Sales%20Reports) documentation to learn how to use Vendor Sales Reports.

## Contributing

This project is licensed under the [Apache 2.0 Software License](https://bitbucket.org/hegyit/vendor-sales-reports/src/master/LICENSE).

### Rules of Engagement

If you want to submit bug fixes or improvements to the Plugin, please assist us by adhering to these simple guidelines:

1. Do not commit directly to the primary branch. Please either fork this repo or use a 'feature branch' and then create a Pull Request for your changes to be accepted.
2. Create an Issue for any changes and prefix your commit messages with the relevant issue key.
3. You are responsible for testing and verifying your own changes. Please be diligent in this and help make life better for everyone who has to work on this project! :)
